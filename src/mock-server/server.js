const express = require('express');
const cors = require('cors');
const router = express.Router();
const allBooks = require('./AllBooks');
let app = express();

app.use(cors());

router.get('/api/books/:rowsPerPage/:pageIndex', (req, res) => {
  const { params } = req;
  const pageIndex = parseInt(params.pageIndex);
  const rowsPerPage = parseInt(params.rowsPerPage);
  const totalDataLength = allBooks.length;
  const firstBookIndex = pageIndex * rowsPerPage;
  const lastBookIndex = firstBookIndex + rowsPerPage > totalDataLength ? totalDataLength : firstBookIndex + rowsPerPage;
  const dataToSend = allBooks.slice(firstBookIndex, lastBookIndex);
  res.send({ data: dataToSend, totalDataLength });
});

app.use(router);
app.listen(4242, () => console.log(`Example app listening on port ${4242}`));
