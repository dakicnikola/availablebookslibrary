import React, { Component } from 'react';
import { DATA_ITEM } from "../MainGrid/MainGrid";
import { Fab, Input } from "@material-ui/core";

export type PROPS = {
  bookData: DATA_ITEM
};

class BookPreview extends Component<any> {
  render() {
    const { changeQuantity, bookData } = this.props;
    const { name: title, writer: author, year, pages, quantity, photo } = this.props.bookData;
    return (
      <div className={'bookPreviewContainer'}>
        <div>
          <div className={'headerSection'}>
            <div className={'title'}>{title}</div>
            <div className={'author'}>{author}</div>
          </div>
        </div>
        <div className={'photoSection'}>
          <img src={photo} alt="" />
        </div>
        <div className={'infoSection'}>
          <table>
            <thead>
            <tr>
              <th>Info</th>
              <th />
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>Title</td>
              <td>{title}</td>
            </tr>
            <tr>
              <td>Author</td>
              <td>{author}</td>
            </tr>
            <tr>
              <td>Year</td>
              <td>{new Date(year).getFullYear()}</td>
            </tr>
            <tr>
              <td>Pages</td>
              <td>{pages}</td>
            </tr>
            <tr>
              <td>Quantity</td>
              <td>
                <Input value={quantity} onChange={changeQuantity} type={'number'} /></td>
            </tr>
            </tbody>
          </table>
          <div>
          </div>
        </div>
      </div>
    )
  }
}

export default BookPreview;
