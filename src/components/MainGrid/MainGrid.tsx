import React from 'react';
import axios from "axios";
import {
  TableBody,
  TableHead,
  Table,
  TableRow,
  TableCell,
  TableFooter,
  TablePagination,
  TextField
} from "@material-ui/core";
import arrowUp from '../../arrow-up.png';
import arrowDown from '../../arrow-down.png';

export type PROPS = {
  changeSelected: Function,
  setChangeQuantityFn: Function
};
export type STATE = {
  data: any[],
  selectedId: number | undefined,
  currentPage: number,
  rowsPerPage: number,
  totalDataLength: number,
  lastDataIndexSorted: string,
  sortDirection: boolean,
  filterText: string
}
export type DATA_ITEM = {
  id: number,
  name: string,
  writer: string,
  year: string,
  pages: number,
  quantity: number,
  photo: string,
  writerBirthDate: string
}
export type COLUMN = {
  name: string,
  dataIndex: string
}

class MainGrid extends React.Component<PROPS, STATE> {

  state: STATE = {
    data: [],
    selectedId: undefined,
    currentPage: 0,
    rowsPerPage: 25,
    totalDataLength: 0,
    sortDirection: false,
    lastDataIndexSorted: '',
    filterText: ''
  };

  columns: COLUMN[] = [
    { name: '', dataIndex: 'photo' },
    { name: 'Title', dataIndex: 'name' },
    { name: 'Author', dataIndex: 'writer' },
    { name: 'Year', dataIndex: 'year' },
    { name: 'Pages', dataIndex: 'pages' },
    { name: 'Quantity', dataIndex: 'quantity' }
  ];

  componentDidMount(): void {
    this.getAndLoadData();
    this.props.setChangeQuantityFn(this.changeQuantityToData);
  }


  /**
   * Filters data on render
   */
  filterData = (data: DATA_ITEM[], filterText: string) => {
    return data.filter(this.filterByName.bind(this, filterText.toLowerCase()))
  };

  /**
   * Returns true if name property of dataItem includes filterText (case insensitive)
   */
  filterByName = (filterText: string, dataItem: DATA_ITEM) => dataItem.name.toLowerCase().includes(filterText);

  /**
   * Gets books from server
   */
  getAndLoadData = () => {
    const { rowsPerPage, currentPage } = this.state;
    axios.get('http://localhost:4242/api/books/' + rowsPerPage + '/' + currentPage)
      .then(this.loadData)
      .catch((error) => {
        console.log(error);
      });
  };

  /**
   * Sets data to state on request success
   */
  loadData = (response: any) => {
    const { data, totalDataLength } = response.data;
    this.setState({ data, totalDataLength });
  };

  /**
   * Handles page changes, updates state and refreshes data
   */
  handleChangePage = (e: any, page: number) => {
    this.setState({ currentPage: page, lastDataIndexSorted: '' },
      this.getAndLoadData
    );
  };

  /**
   *  Sets filter text to state
   */
  handleFilterChange = (e: any) => {
    this.setState({ filterText: e.target.value })
  };

  /**
   * Handles numberOfRowsPerPage change, updates state and refreshes data
   */
  handleRowsPerPageChange = (event: any) => {
    const rowsPerPage = event.target.value;
    this.setState({ rowsPerPage, currentPage: 0, lastDataIndexSorted: '' },
      this.getAndLoadData
    );
  };

  /**
   * Row click event handler, manages selection
   */
  onRowClick = (rowData: DATA_ITEM) => {
    this.setState({ selectedId: rowData.id });
    this.props.changeSelected(rowData);
  };

  /**
   * Finds dataItem by id in the table and sets it's new quantity,
   * also passes that dataItem back to parent to update selectedDataItem
   */
  changeQuantityToData = (dataItem: DATA_ITEM) => {
    const data = [...this.state.data];
    let i = data.length;
    while (i--) {
      if (dataItem.id === data[i].id) {
        data[i].quantity = dataItem.quantity;
        break;
      }
    }
    this.setState({ data },
      this.props.changeSelected.bind(this, dataItem));
  };

  /**
   * Used for sorting as comparative function between two items in table
   * @param dataIndex -> property used for comparing items
   * @param direction -> sort direction
   * @param item1
   * @param item2
   */
  sortByDataIndex = (dataIndex: string, direction: boolean, item1: DATA_ITEM, item2: DATA_ITEM) => {
    return ((item1 as any)[dataIndex] > (item2 as any)[dataIndex]) ? (direction ? 1 : -1) : (direction ? -1 : 1)
  };

  /**
   * Sorts data by column dataIndex
   */
  sortDisplayedDataByDataIndex = (dataIndex: string) => {
    const data = [...this.state.data];
    let { lastDataIndexSorted, sortDirection: dir } = this.state;
    let sortDirection;
    if (dataIndex === lastDataIndexSorted) {
      sortDirection = !dir;
    } else {
      lastDataIndexSorted = dataIndex;
      sortDirection = dir;
    }
    data.sort(this.sortByDataIndex.bind(this, dataIndex, sortDirection));
    this.setState({ lastDataIndexSorted, sortDirection, data })
  };

  /**
   * Renders table-body cells
   */
  renderBodyCell = (rowData: DATA_ITEM, { dataIndex }: COLUMN) => {
    let innerCell;
    switch (dataIndex) {
      case 'year':
        innerCell = new Date(rowData.year).getFullYear();
        break;
      case 'photo':
        innerCell = <img src={rowData.photo} alt="" style={{
          height: 30,
          textAlign: 'center'
        }} />;
        break;
      default:
        innerCell = (rowData as any)[dataIndex];
        break;
    }
    return (<TableCell key={rowData.id + '-' + dataIndex}>{innerCell}</TableCell>)
  };

  /**
   * Renders header table cells
   * Adds click event handlers to name, writer and pages columns for sorting
   */
  renderHeaderCell = (column: COLUMN) => {
    const { dataIndex, name } = column;
    let cell;
    switch (dataIndex) {
      case 'name':
      case 'writer':
      case 'pages':
        const { lastDataIndexSorted, sortDirection } = this.state;
        const placeIcon = lastDataIndexSorted === dataIndex;
        cell = (<TableCell key={dataIndex}
                           onClick={this.sortDisplayedDataByDataIndex.bind(this, dataIndex)}>
          {placeIcon && <img src={sortDirection ? arrowUp : arrowDown} alt="" />}
          {name}
        </TableCell>);
        break;
      default:
        cell = <TableCell key={dataIndex}>{name}</TableCell>;
        break;
    }
    return cell;
  };

  /**
   * Renders table-body row
   */
  renderTableRow = (rowData: DATA_ITEM) => {
    const isSelected = rowData.id === this.state.selectedId;
    return (<TableRow
      key={rowData.id}
      onClick={this.onRowClick.bind(this, rowData)}
      className={isSelected ? 'selected' : ''}
    >
      {this.columns.map(this.renderBodyCell.bind(this, rowData))}
    </TableRow>)
  };

  render() {
    const { data, currentPage, rowsPerPage, totalDataLength, filterText } = this.state;
    const filteredData = this.filterData([...data], filterText);
    return (
      <div className={'mainGrid'}>
        <div className={'gridHeader'}>
          <TextField id="standard-basic"
                     label="Filter list"
                     onChange={this.handleFilterChange}
                     value={filterText}
                     color={'secondary'}
                     type={'search'}
          />
          <span>Books</span>
        </div>
        <div className="tableWrapper">
          <Table>
            <TableHead>
              <TableRow>
                {this.columns.map(this.renderHeaderCell)}
              </TableRow>
            </TableHead>
            <TableBody>
              {filteredData.map(this.renderTableRow)}
            </TableBody>
            <TableFooter>
              <tr>
                <TablePagination
                  count={totalDataLength}
                  onChangePage={this.handleChangePage}
                  page={currentPage}
                  rowsPerPage={rowsPerPage}
                  onChangeRowsPerPage={this.handleRowsPerPageChange}
                />
              </tr>
            </TableFooter>
          </Table>
        </div>
      </div>
    )
  }
}

export default MainGrid;
