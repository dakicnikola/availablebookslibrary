import React, { Component } from 'react';
import './Styles.scss';
import MainGrid, { DATA_ITEM } from "./components/MainGrid/MainGrid";
import BookPreview from "./components/BookPreview/BookPreview";

class App extends Component {

  state = {
    selectedBook: undefined,
  };
  editSelected: any;

  /**
   * Sets selected book from mainGrid table to state
   */
  handleBookSelection = (bookData: DATA_ITEM) => {
    this.setState({ selectedBook: bookData })
  };

  /**
   * Helper function to be able to change data on grid, set on mainGrid mount
   */
  setEditQuantity = (editSelected: Function) => {
    this.editSelected = editSelected;
  };

  /**
   * Passes selectedBook dataItem to grid, to change quantity there
   */
  changeQuantity = (e: any) => {
    let { value } = e.target;
    value = parseInt(value);
    if (!value || value < 0) {
      value = 0;
    }
    const { selectedBook } = this.state;
    //@ts-ignore
    this.editSelected({ ...selectedBook, quantity: value });
  };

  render() {
    const { selectedBook } = this.state;
    return (
      <div className="App">
        <MainGrid changeSelected={this.handleBookSelection} setChangeQuantityFn={this.setEditQuantity} />
        {selectedBook && <BookPreview bookData={selectedBook} changeQuantity={this.changeQuantity} />}
      </div>
    );
  }
}

export default App;
